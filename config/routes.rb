Brief::Application.routes.draw do
  resources :forms do
    resources :comments
  end
  root "index#index"

end
