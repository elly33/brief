class AddImageColumnsToForm < ActiveRecord::Migration
  def self.up
    change_table :forms do |t|
      t.has_attached_file :image
    end
  end

  def self.down
    drop_attached_file :forms, :image
  end
end
