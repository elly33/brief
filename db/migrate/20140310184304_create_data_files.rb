class CreateDataFiles < ActiveRecord::Migration
  def change
    create_table :data_files do |t|
      t.text :path
      t.references :form, index: true

      t.timestamps
    end
  end
end
