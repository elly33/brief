class AddAvatarColumnsToForms < ActiveRecord::Migration
  def self.up
    change_table :forms do |t|
      t.has_attached_file :avatar
    end
  end

  def self.down
    drop_attached_file :forms, :avatar
  end
end
