class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :title
      t.string :service
      t.string :type
      t.datetime :date
      t.string :email
      t.text :text

      t.timestamps
    end
  end
end
