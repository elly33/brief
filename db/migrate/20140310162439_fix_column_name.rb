class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :forms, :type, :kind
  end
end
