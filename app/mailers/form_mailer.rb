class FormMailer < ActionMailer::Base
  default from: "info@rocketmind.ru"

  def new_request(form)
    @form = form
    mail(:to=>form.email, :subject => "New request!")
  end
end
