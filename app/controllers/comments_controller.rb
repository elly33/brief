class CommentsController < ApplicationController
  def create
    @form = Form.find(params[:form_id])
    @comment = @form.comments.create(params[:comment].permit(:commenter, :body))
    redirect_to form_path(@form)
  end

  def destroy
    @form = Form.find(params[:form_id])
    @comment = @form.comments.find(params[:id])
    @comment.destroy
    redirect_to form_path(@form)
  end
end
