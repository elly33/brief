class FormsController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "adminpass", only: [ :show]

  def index
    @forms = Form.all
  end

  def new
    @form = Form.new
  end

  def show
    @form = Form.find(params[:id])
    MainsmsApi::Configuration.setup project: 'Brief', api_key: 'a968e12fd7c59'
    message = MainsmsApi::Message.new(sender: 'sendertest', message: 'From Russia with love', recipients: ['89051492237'])
    response = message.deliver
  end

  def create
    @form = Form.new(form_params)

    if @form.save
      # Tell the FormMailer to send a welcome Email after save
      FormMailer.new_request(@form).deliver
      #Send sms
      SMS.text :message => "Hello!", :to => "+79045958616"
      #form.html { redirect_to(action: :show, id: @form.id, notice: 'Request was successfully created.') }
     # form.json { render json: @form, status: :created, location: @form }
      #redirect_to action: :show, id: @form.id
      render 'show'
    else
      render 'new'
    end
  end

  def edit
    @form = Form.find(params[:id])
  end

  def update
    @form = Form.find(params[:id])

    if @form.update(params[:form].permit(:title, :email, :service, :kind, :style, :content, :text, :image))
      redirect_to @form
    else
      render 'edit'
    end
  end

  def destroy
    @form = Form.find(params[:id])
    @form.destroy

    redirect_to action: :index
  end

  private

  def form_params
    params.require(:form).permit(:title, :email, :service, :kind, :style, :content, :image, :text)
  end
end
