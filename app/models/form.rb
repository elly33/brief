class Form < ActiveRecord::Base
  #attr_accessor :image_file_name, :image_content_type, :image_file_size, :image_updated_at

  has_many :comments, dependent: :destroy
  has_one :data_file
  has_attached_file :image,
                    :styles => { :medium => "300x300>", :thumb => "100x100>" },
                    :path => ":rails_root/public/attachments/:attachment/:id/:style/:filename",
                    :url => "/attachments/:attachment/:id/:style/:filename",
                    :default_url => "/images/:style/missing.png"
  validates :title, presence: true,length: { minimum: 5 }
  validates :email, email_format: { message: "doesn't look like an email address" }
  validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)

  # send email
 # after_create do |form|
    #FormMailer.new_request(form).deliver
  #end

end
