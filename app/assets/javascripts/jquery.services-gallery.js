if(!window.application){
	window.application = {};
}

window.application.servicesGallery = function(){
	'use strict';

	function checkContentHeights(){
		return servicesSlides.filter(function(){
			return winHeight - parseInt(main.css('paddingTop')) < $(this).height();
		}).length ? true : false;
	}

	function initGallery(){
		if (!services.find('.tab').hasClass('gallery-js-ready')) {
			services.find('.tab').scrollGallery({
				mask: 'div.gallery',
				generatePagination: '.pager',
				stretchSlideToMask: true
			});
		}
	}

	function destroyGallery(){
		if (services.find('.tab').hasClass('gallery-js-ready')) {
			services.find('.tab').each(function(){
				var cur = jQuery(this);
				cur.data('ScrollGallery').destroy();
				setTimeout(function(){
					cur.find('.slider').css({
						marginLeft: '',
						width: ''
					}).find('.item-info').css('width', '');
				}, 100);
			});
		}
	}

	var bodyObj = jQuery('body'),
		htmlBodyObj = jQuery('html').add(bodyObj),
		maxWidth = 924,
		services = jQuery('.services-info'),
		servicesSlides = services.find('.item'),
		firstBox = services.find('.item-info').eq(0),
		secondBox = services.find('.gallery'),
		main = jQuery('#main'),
		win = jQuery(window),
		btns = services.find('.btn-holder'),
		disActiveClass = 'no-gallery',
		winHeight = win.height(),
		switcher = services.find('.switcher'),
		switcherBtns = switcher.find('a'),
		isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

	servicesSlides.height('');
	services.addClass(disActiveClass);
	if (checkContentHeights()) {
		services.removeClass(disActiveClass);
		servicesSlides.height(winHeight - parseInt(main.css('paddingTop')));
	}

	if (typeof jQuery.fn.contentTabs == 'function') {
		jQuery('.services-info .switcher').contentTabs({
			addToParent:true,
			effect: 'fade'
		});
	}

	switcherBtns.on({
		'click': function(event){
			event.preventDefault();
			if (!servicesSlides.hasClass('active') && switcher.parent().css('position') == 'absolute') {
				switcherBtns.closest('li').removeClass('active').has(this).addClass('active');
				servicesSlides.addClass('active');
				firstBox.fadeOut(500, function(){
					secondBox.show();
					win.trigger('resize');
					secondBox.hide();
					secondBox.fadeIn(500, function(){
						win.trigger('resize');
					});
				});
			}
		}
	});

	win.on({
		'resize orientationchange ready load': function(){
			if (btns.css('right') != 0) {
				winHeight = win.height();
				servicesSlides.height('');
				services.addClass(disActiveClass);
				if (!checkContentHeights()) {
					services.removeClass(disActiveClass);
					servicesSlides.height(winHeight - parseInt(main.css('paddingTop')));
				}
			} else {
				servicesSlides.height('');
				services.addClass(disActiveClass);
				firstBox.show();
			}
			if (btns.css('display') !== 'none') {
				initGallery();
			} else {
				destroyGallery();
			}
		}
	});

	initGallery();
}