if(!window.application){
	window.application = {};
}

window.application.initPlugins = function(){
	'use strict';

	var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
	
	jQuery('.top-bar img').load(function(){
		var curImg = jQuery(this),
			curImgData = {
				width: /*parseInt(curImg.data('max-width')) || 0*/ 256,
				height: /*parseInt(curImg.data('max-height')) || 0*/ 256
			};
		curImg.attr(curImgData);
		jQuery(window).trigger('resize');
		// if (curImgData.width < parseInt(curImg.css('width')) && curImgData.height < parseInt(curImg.css('height'))) {
		// 	curImgData = {
		// 		width: parseInt(curImg.css('width')),
		// 		height: parseInt(curImg.css('height'))
		// 	}
		// 	curImg.css(curImgData);
		// 	curImg.data('max-width', curImgData.width);
		// 	curImg.data('max-height', curImgData.height);
		// 	jQuery(window).trigger('resize');
		// }
	});

	if (typeof jQuery.fn.scrollGallery === 'function') {
		/*jQuery('.visual').scrollGallery({
			mask: 'div.holder',
			generatePagination: '.pager',
			stretchSlideToMask: true,
			useTranslate3D: true
		});*/
	}
	if (typeof jQuery.fn.fadeGallery === 'function') {
		jQuery('.ill-gallery').fadeGallery({
			slides: 'ul > li',
			activeClass:'active',
			disableFadeIE: true,
			autoRotation: true,
			pauseOnHover: false,
			autoHeight: true,
			onInit: function(){
				var tempArr = [];
				this.slides.not(this.slides.eq(this.currentIndex)).each(function(){
					tempArr.push(this.getAttribute('data-color'));
				});
				this.gallery.addClass(this.slides.eq(this.currentIndex).attr('data-color')).removeClass(tempArr.join(' '));
			},
			onBeforeChange: function(){
				var tempArr = [];
				this.slides.not(this.slides.eq(this.currentIndex)).each(function(){
					tempArr.push(this.getAttribute('data-color'));
				});
				this.gallery.addClass(this.slides.eq(this.currentIndex).attr('data-color')).removeClass(tempArr.join(' '));
			},
			animSpeed: 500
		});
		jQuery(window).on('load', function(){
			jQuery('.ill-gallery').css('opacity', 1);
		})
	}
	if (typeof jQuery.fn.imgStretch == 'function') {
		jQuery('.map img').imgStretch();
		jQuery(window).on({
			'load ready orientationchange resize': function(){
				jQuery('.map img').imgStretch();
			}
		})
	}
	if (typeof initAutoScalingNav == 'function') {
		jQuery(window).on({
			'load': function(){
				initAutoScalingNav({
					menuId: "map-nav",
					flexible: true
				});
			}
		});
	}
	if(typeof jQuery.fn.customSelect == 'function'){
		$('.feedback select').customSelect({
			selectStructure: '<div class="selectArea"><div class="left"></div><div class="center"></div><a href="#" class="selectButton"><i class="ico">&nbsp;</i></a><div class="disabled"></div></div>',
			defaultText: function(select){
				return select.getAttribute('data-placeholder');
			},
			withWindowScroll: true,
			touchDropDefault: true
		});
	}
	if(typeof jQuery.fn.formValidator == 'function'){
		jQuery('.feedback').formValidator({
			addClassAt: '.box',
			onSubmit: function(){
				var scrollTop = parseInt(jQuery(window).scrollTop());
				setTimeout(function(){
					jQuery('html, body').animate({
						'scrollTop': scrollTop + 1
					}, 100);
				}, 500);
			}
		});
	}

	var items = jQuery('.services .list > li');
	if (isTouchDevice) {
		items.on({
			'click': function(){
				if (jQuery(this).hasClass('hovered')) {
					items.removeClass('hovered');
				} else {
					items.removeClass('hovered').filter(this).addClass('hovered');
				}
			}
		});
	} else {
		items.on({
			'mouseenter': function(){
				jQuery(this).addClass('hovered');
			},
			'mouseleave': function(){
				jQuery(this).removeClass('hovered');
			}
		})
	}
}