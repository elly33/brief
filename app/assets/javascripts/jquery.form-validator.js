/*--- form validator ---*/
(function($){
	"use strict";

	function FormValidator(thisDOMObj, config){
		this.form = jQuery(thisDOMObj);
		if (this.form.data('FormValidator') && typeof this.form.data('FormValidator')[config] === 'function') { // call api function
			this.form.data('FormValidator')[config]();
		} else if (typeof config != 'string') { // init custom form
			// default options
			this.options = jQuery.extend({
				items: 'input:text, input[type="email"], textarea', // validate items
				errorClass: 'error', // error class
				successClass: 'success', // success class
				validAttr: 'type', // attribute for type of regulation exemptions
				regExp: {
					text: /^/,
					email: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
					number: /^\d+$/
				}, // type of regulation exemptions
				defaultExp: 'text', // default name type of regulation exemptions
				minLength: 3, // min length of value in items
				addClassAt: false, // filter for parent of validate items
				onlyFocused: false, // validate only focused item
				eventObj: false, // event object item
				event: 'submit', // event type
				preventDefault: true, // prevent default when validation not passed
				beforeValid: null, // before check validation callback
				afterValid: null, // after check validation callback
				onSubmit: null, // on submit callback
				onInit: null // on init plugin callback
			}, config);

			this.init();
		}
		return this;
	}

	FormValidator.prototype = {
		// init function
		init: function(){
			// add api in data form
			this.form.data('FormValidator', this);

			this.createElements();
			this.attachEvents();

			// init callback
			if (typeof this.options.onInit == 'function') {
				this.bindScope(this.options.onInit)(this.getUI());
			}
		},
		getUI: function(){
			return {
				form: this.form,
				errorItems: this.errorItems,
				successItems: this.successItems,
				passValid: this.passValid
			};
		},
		// attach events and listeners
		attachEvents: function(){
			this.eventHandler = this.bindScope(function(event){
				// before validation callback
				if (typeof this.options.beforeValid == 'function') {
					this.bindScope(this.options.beforeValid)(event, this.getUI());
				}

				if (this.options.preventDefault && event.type == 'submit') {
					this.checkItems();
				} else if (this.items.filter(event.currentTarget).length) {
					this.checkItems(this.options.onlyFocused ? event.currentTarget : false);
				}

				if (!this.passValid && this.options.preventDefault && event.type == 'submit') {
					event.preventDefault();
				}

				// after validation callback
				if (typeof this.options.afterValid == 'function') {
					this.bindScope(this.options.afterValid)(event, this.getUI());
				}

				// on submit callback
				if (typeof this.options.onSubmit == 'function' && event.type == 'submit') {
					this.bindScope(this.options.onSubmit)(event, this.getUI());
				}
			});

			if (this.options.eventObj) {
				this.eventObj.on(this.options.event, this.eventHandler);
				this.form.on('submit', this.eventHandler);
			} else {
				this.form.on(this.options.event, this.eventHandler);
			}
		},
		// create api elements
		createElements: function(){
			this.successItems = jQuery();
			this.errorItems = jQuery();
			if (this.options.eventObj) {
				this.eventObj = this.form.find(this.options.eventObj);
			}
			if (this.options.addClassAt) {
				this.addAtItems = this.form.find(this.options.addClassAt);
			}
			this.items = this.form.find(this.options.items);
			this.passValid = false;
		},
		// api update function
		update: function(){
			this.items.removeClass(this.options.errorClass + ' ' + this.options.successClass);
			if (this.options.eventObj) {
				this.eventObj.off(this.options.event, this.eventHandler);
			} else {
				this.form.off(this.options.event, this.eventHandler);
			}

			this.createElements();
			this.attachEvents();

			// init callback
			if (typeof this.options.onInit == 'function') {
				this.bindScope(this.options.onInit)(this.getUI(), true);
			}
		},
		refreshState: function(item){
			if (this.options.addClassAt) {
				this.errorItems.each(this.bindScope(function(index, thisItem){
					this.addAtItems.has(thisItem).removeClass(this.options.successClass).addClass(this.options.errorClass);
				}));
				this.successItems.each(this.bindScope(function(index, thisItem){
					this.addAtItems.has(thisItem).removeClass(this.options.errorClass).addClass(this.options.successClass);
				}));
			} else {
				this.errorItems.removeClass(this.options.successClass).addClass(this.options.errorClass);
				this.successItems.removeClass(this.options.errorClass).addClass(this.options.successClass);
			}
		},
		checkItems: function(item){
			if (!item) {
				this.successItems = this.items.filter(this.bindScope(function(index, thisItem){
					var curType = thisItem.getAttribute(this.options.validAttr);
					if (thisItem.value.length >= this.options.minLength) {
						if (this.options.regExp[curType] && curType != this.options.defaultExp) {
							return this.options.regExp[curType].test(thisItem.value);
						} else if (this.options.regExp[this.options.defaultExp]) {
							return this.options.regExp[this.options.defaultExp].test(thisItem.value);
						}
					} else {
						return false;
					}
				}));
				this.errorItems = this.items.not(this.successItems);
				this.refreshState(item);
				this.passValid = this.errorItems.length ? false : true;
			} else {
				this.passValid = jQuery(item).addClass(this.options.errorClass).removeClass(this.options.successClass).filter(this.bindScope(function(index, thisItem){
					var curType = thisItem.getAttribute(this.options.validAttr);
					if (thisItem.value && thisItem.value.length >= this.options.minLength) {
						if (this.options.regExp[curType] && curType != this.options.defaultExp) {
							return this.options.regExp[curType].test(thisItem.value);
						} else if (this.options.regExp[this.options.defaultExp]) {
							return this.options.regExp[this.options.defaultExp].test(thisItem.value);
						}
					} else {
						return false;
					}
				})).addClass(this.options.successClass).removeClass(this.options.errorClass).length ? false : true;
			}
			return this.passValid;
		},
		// api destroy function
		destroy: function(){
			this.items.removeClass(this.options.errorClass + ' ' + this.options.successClass);
			if (this.options.eventObj) {
				this.eventObj.off(this.options.event, this.eventHandler);
				this.form.off('submit', this.eventHandler);
			} else {
				this.form.off(this.options.event, this.eventHandler);
			}
		},
		bindScope: function(func, scope){
			return jQuery.proxy(func, scope || this);
		}
	};

	jQuery.fn.formValidator = function(config){
		return this.each(function(){
			new FormValidator(this, config);
		});
	};
}(jQuery));