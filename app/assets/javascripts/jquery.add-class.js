if(!window.application){
	window.application = {};
}

window.application.addClass = function(){
	'use strict';

	function updateState(funcName){
		drop.removeClass(classNames).addClass(funcName);
	}

	var holder = jQuery('#header'),
		opener = jQuery('.open-top-bar'),
		drop = jQuery('.top-bar'),
		activeClass = 'opened',
		funcAttr = 'data-func',
		classNames = 'subscribe-show feedback-show cols-show',
		bodyObj = jQuery('body'),
		projectItems = jQuery('.projects > ul > li'),
		win = jQuery(window),
		isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		aboutGallery = jQuery('.about-gallery'),
		btnPrev = aboutGallery.find('.btn-prev'),
		btnNext = aboutGallery.find('.btn-next'),
		slider = aboutGallery.find('.slider');
	btnPrev.on({
		'click': function(event){
			event.preventDefault();
			slider.addClass('active');
		}
	});
	btnNext.on({
		'click': function(event){
			event.preventDefault();
			slider.removeClass('active');
		}
	});
	if (typeof jQuery.fn.mCustomScrollbar == 'function') {
		drop.mCustomScrollbar({
			scrollInertia: 300,
			mouseWheelPixels: 100,
			advanced: {
				autoScrollOnFocus: false
			}
		});
	}
	holder.add(opener).on({
		'click': function(event){
			if (!$(event.target).closest('.logo').length) {
				event.preventDefault();
				if (holder.hasClass(activeClass)) {
					holder.removeClass(activeClass);
					drop.stop().animate({
						opacity: 0,
						top: '-100%'
					}, 400, function(){
						drop.css('z-index', '-20');
					});
				} else {
					updateState(jQuery(this).attr(funcAttr));
					win.trigger('resize');
					holder.addClass(activeClass).removeClass('hide');
					drop.css('z-index', '9').stop().animate({
						opacity: 1,
						top: holder.outerHeight()
					}, 400);
				}
			}
		}
	});

	win.on({
		'resize ready load orientationchange': function(){
			drop.removeClass('scroll').css('height', '');
			var heightDrop = win.height() - holder.outerHeight();
			if (drop.outerHeight() > heightDrop) {
				drop.addClass('scroll').css('height', '100%');
			} else if(drop.hasClass('scroll')) {
				drop.removeClass('scroll').css('height', '');
			}
			drop.mCustomScrollbar('update');
		}
	});

	projectItems.on({
		'mouseenter': function(){
			projectItems.removeClass('hovered');
			jQuery(this).addClass('hovered');
		},
		'mouseleave': function(){
			projectItems.removeClass('hovered');
		}
	})
}