(function($){
	$.fn.imgStretch = function(){
		return this.each(function(){
			var cur = jQuery(this).css({
					width: '',
					height: ''
				}),
				curWidth = cur.width(),
				curHeight = cur.height(),
				parent = cur.parent(),
				parentWidth = parent.width(),
				parentHeight = parent.height(),
				diffWidth = curWidth/parentWidth,
				diffHeight = curHeight/parentHeight,
				diffCur = curHeight/curWidth;
			if(diffWidth < diffHeight){
				cur.css({
					width: parentWidth,
					height: parentWidth*diffCur,
					marginTop: -(parentWidth*diffCur - parentHeight)/2,
					marginLeft: 0
				});
			}
			else{
				cur.css({
					height: parentHeight,
					width: parentHeight/diffCur,
					marginLeft: -(parentHeight/diffCur - parentWidth)/2,
					marginTop: 0
				});
			}
		});
	};
}(jQuery));