if(!window.application){
	window.application = {};
}

window.application.mainGallery = function(){
	'use strict';

	function updateImgSize(load){
		if(load || bodyObj.hasClass('loaded')){
			if(typeof jQuery.fn.imgStretch === 'function'){
				jQuery('.visual .img-holder img').imgStretch();
			}
		}
	}

	function onImgLoad(){
		if(!bodyObj.hasClass('loaded')){
			updateImgSize(true);
			bodyObj.addClass('loaded');
		}
	}

	function onScrolling(ui){
		if(btnProjects.css('top') != '-9999px' && bodyObj.hasClass(noGalleryClass)){
			if (btnProjectsTop > (-ui.winScroll) + winHeight/2) {
				if (!btnProjects.hasClass('show')) {
					btnProjects.addClass('show');
					btnProjects.fadeIn(500);
				}
			} else {
				if (btnProjects.hasClass('show')) {
					btnProjects.removeClass('show');
					btnProjects.fadeOut(500);
				}
			}
		}
		var winFlag = typeof ui.winScroll !== 'undefined',
			scrollTop = -(winFlag ? ui.winScroll : parseInt(ui.slider[0].style[ui.animProperty])),
			setHeader = winFlag ? (scrollTop > ui.stepOffsets[1] - headerHeight + 29) : (-(scrollTop + headerHeight) < ui.stepOffsets[1]);
		if (isTouchDevice) {
			if (scrollTop > 50) {
				header.addClass('white');
			} else {
				header.removeClass('white');
			}
		}
		if (setHeader) {
			if (!header.hasClass(headerActiveClass)) header.addClass(headerActiveClass);
		} else {
			if (header.hasClass(headerActiveClass)) header.removeClass(headerActiveClass);
			if (!isTouchDevice) {
				visualSection.css(ui.animProperty, (scrollTop / visualAlpha) < 1 ? 0 : (scrollTop / visualAlpha));
			}
		}
	}

	function showItem(step){
		if(!wrapperSlides.eq(step).hasClass('show')){
			wrapperSlides.eq(step).addClass('show');
		}
	}

	function initWrapperGallery(){
		if (!checkContentHeights()) {
			bodyObj.removeClass(noGalleryClass);
			if (parseInt(headerContainer.css('max-width')) >= 924) {
				if (typeof jQuery.fn.scrollGallery === 'function') {
					wrapper.scrollGallery({
						mask: '#main',
						btnPrev: false,
						btnNext: '.projects header',
						generatePagination: false,
						pagerLinks: false,
						stretchSlideToMask: true,
						circularRotation: false,
						disableWhileAnimating: true,
						vertical: true,
						mousewheel: true,
						useTranslate3D: true,
						easing: 'easeInOutExpo',
						onScrolling: function(){
							onScrolling(this);
						},
						onBeforeChange: function(){
							
						},
						onhalfchange: function(){
							showItem(this.currentStep);
							if (this.currentStep == 2) {
								btnProjects.stop().fadeOut(this.options.animSpeed/3);
							} else if(this.direction == -1) {
								btnProjects.stop().fadeIn(this.options.animSpeed/3);
							}
						},
						onChange: function(){
							onScrolling(this);
						},
						onInit: function(){
							showItem(this.currentStep);
							onScrolling({
								winScroll: -(parseInt(win.scrollTop())),
								animProperty: 'margin-top',
								stepOffsets: [0, winHeight]
							});
						},
						animSpeed: mainAnimSpeed,
						step: 1
					});
				}
			}
		} else {
			bodyObj.addClass(noGalleryClass);
		}
	}

	function updateMasonry(){
		if (masonryData) {
			updateMasonryItemsWidth(masonryData);
			masonryData.layout();
		}
	}

	function updateMasonryItemsWidth(){
		var curProjectWidth = projectItems.eq(0).width('').width(),
			bigItems = projectItems.filter('.big'),
			paddingRight = 0,
			inRow = 2, temp = jQuery();
		masonryHolderWidth = masonryHolder.width('').width();
		switch (parseInt(masonryHolderWidth/curProjectWidth)) {
			case 1:
			case 2:
				inRow = 2;
				break;
			case 3:
			case 4:
				inRow = 4;
				break;
			case 5:
			case 6:
				inRow = 6;
				break;
			default:
				inRow = 8;
		}
		paddingRight = masonryHolderWidth%inRow;
		masonryHolderWidth = masonryHolderWidth + inRow - paddingRight;
		masonryHolder.width(masonryHolderWidth);
		curProjectWidth = masonryHolderWidth/inRow;
		switch (inRow) {
			case 2:
				projectItems.not('.text-box').css({
					height: curProjectWidth,
					width: curProjectWidth
				});
				projectItems.filter('.text-box').css({
					height: '',
					width: ''
				}).find('.box').css('padding-right', '');
				bigItems.css({
					width: curProjectWidth,
					height: curProjectWidth
				});
				break;
			default:
				projectItems.css({
					height: curProjectWidth,
					width: curProjectWidth
				}).find('.box').css('padding-right', paddingRight);
				bigItems.css({
					width: curProjectWidth * 2,
					height: curProjectWidth * 2
				});
		}
		if (inRow == 4) {
			if (bigItems.index() != 1 && masonryData) {
				temp = bigItems.prev().clone();
				masonryData.remove(bigItems.prev()[0]);
				bigItems.after(temp);
				masonryData.addItems(temp);
				masonryData.reloadItems();
				projectItems = jQuery(projectItems.selector);
			}
		} else {
			if (bigItems.index() == 1 && masonryData) {
				temp = bigItems.next().clone();
				masonryData.remove(bigItems.next()[0]);
				bigItems.before(temp);
				masonryData.addItems(temp);
				masonryData.reloadItems();
				projectItems = jQuery(projectItems.selector);
			}
		}
	}

	function checkContentHeights(){
		return wrapperSlides.filter(function(){
			return winHeight < $(this).height();
		}).length ? true : false;
	}

	jQuery('html, body').animate({
		'scrollTop': 0
	}, 0);
	var bodyObj = jQuery('body'),
		htmlBodyObj = jQuery('html').add(bodyObj),
		header = jQuery('#header'),
		headerContainer = header.find('.container'),
		maxWidth = 924,
		headerHeight = header.outerHeight(),
		headerActiveClass = 'active',
		visualAlpha = 2,
		visualSection = jQuery('.visual'),
		wrapper = jQuery('#wrapper'),
		wrapperSlides = wrapper.find('.item'),
		win = jQuery(window),
		winHeight = win.height(),
		noGalleryClass = 'no-gallery',
		isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		masonryData = false,
		btnProjects = jQuery('.projects header'),
		btnProjectsTop = parseInt(btnProjects.offset().top),
		firstSlideAnim = true,
		imgGalleryLoad = visualSection.find('.slider > li:first-child img'),
		projectItems = jQuery('.projects > ul > li'),
		masonryHolder = jQuery('.projects > ul'),
		mainAnimSpeed = 700,
		masonryHolderWidth = '100%';
	if (isTouchDevice) {
		if (parseInt(jQuery(window).scrollTop()) > 50) {
			header.addClass('white');
		} else {
			header.removeClass('white');
		}
	}
	initWrapperGallery();
	btnProjects.on({
		'click': function(){
			if(bodyObj.hasClass(noGalleryClass)){
				htmlBodyObj.stop().animate({
					'scrollTop': parseInt(wrapperSlides.has(btnProjects).offset().top) - headerHeight
				}, mainAnimSpeed);
			}
		}
	});
	wrapperSlides.each(function(){
		var cur = jQuery(this);
		cur.data('offsetTop', parseInt(cur.offset().top));
	});
	if (typeof jQuery.fn.masonry === 'function') {
		updateMasonryItemsWidth();
		masonryHolder.masonry({
			transitionDuration: 0,
			isResizeBound: false,
			itemSelector: '.masonry-item',
			columnWidth: '.grid-sizer'
		});
		masonryData = masonryHolder.data('masonry');
		jQuery('.projects > ul img').load(function(){
			updateMasonry();
		});
	}
	win.on({
		'load': function(){
			onImgLoad();
		}
	});
	win.on({
		'load resize orientationchange ready': function(){
			winHeight = win.height();
			visualSection.parent().height(win.height());
			updateImgSize();
			headerHeight = header.outerHeight();
			wrapperSlides.each(function(){
				var cur = jQuery(this);
				cur.data('offsetTop', parseInt(cur.offset().top));
			});
			if (parseInt(headerContainer.css('max-width')) < 924) {
				if(wrapper.hasClass('gallery-js-ready')){
					wrapper.data('ScrollGallery').destroy();
					wrapper.find('.main-slider').css('height', '');
					wrapperSlides.css('height', '');
					setTimeout(function(){
						wrapper.find('.main-slider').css('height', '');
						wrapperSlides.css('height', '');
					}, 100);
					onScrolling({
						winScroll: 0,
						animProperty: 'margin-top',
						stepOffsets: [0, winHeight]
					});
				}
			} else {
				if (!wrapper.hasClass('gallery-js-ready')) {
					initWrapperGallery();
				}
			}
			updateMasonry();
			btnProjectsTop = parseInt(btnProjects.offset().top);
		},
		'scroll mousewheel': function(event, dir){
			if (!header.hasClass('opened')) {
				var scrollTop = parseInt(win.scrollTop()),
					prevTop = win.data('prevTop') || scrollTop;
				if (!dir) {
					dir = scrollTop < prevTop ? 1 : -1;
				}
				if (htmlBodyObj.hasClass('animated')) {
					event.preventDefault();
				}
				else if (dir > 0 && scrollTop <= winHeight - 40 && !isTouchDevice && bodyObj.hasClass(noGalleryClass)) {
					event.preventDefault();
					if (!htmlBodyObj.hasClass('animated')) {
						htmlBodyObj.addClass('animated').animate({
							'scrollTop': dir < 0 ? winHeight - 40 : 0
						}, {
							duration: mainAnimSpeed,
							easing: 'easeInOutExpo',
							step: function(val){
								onScrolling({
									winScroll: -val,
									animProperty: 'margin-top',
									stepOffsets: [0, winHeight]
								});
							},
							complete: function(){
								firstSlideAnim = false;
								htmlBodyObj.removeClass('animated');
							}
						});
					}
				}

				if (!isTouchDevice && bodyObj.hasClass(noGalleryClass)) {
					if (scrollTop >= winHeight - 40) {
						firstSlideAnim = false;
					} else if (!htmlBodyObj.hasClass('animated')) {
						event.preventDefault();
						if (dir) {
							htmlBodyObj.addClass('animated').animate({
								'scrollTop': dir < 0 ? winHeight - 40 : 0
							}, {
								duration: mainAnimSpeed,
								easing: 'easeInOutExpo',
								step: function(val){
									onScrolling({
										winScroll: -val,
										animProperty: 'margin-top',
										stepOffsets: [0, winHeight]
									});
								},
								complete: function(){
									firstSlideAnim = false;
									htmlBodyObj.removeClass('animated');
								}
							});
						}
					}
				}
				if (parseInt(headerContainer.css('max-width')) < 924 || bodyObj.hasClass(noGalleryClass) && !htmlBodyObj.hasClass('animated')) {
					onScrolling({
						winScroll: -scrollTop,
						animProperty: 'margin-top',
						stepOffsets: [0, winHeight]
					});
				}
				wrapperSlides.filter(function(){
					return jQuery(this).data('offsetTop') < (scrollTop + winHeight/2);
				}).each(function(index){
					showItem(index);
				});
				win.data('prevTop', scrollTop);
			}
		}
	});
	if (isTouchDevice && jQuery.fn.hammer) {
		wrapper.find('#main').hammer({
			drag_block_vertical: true,
			drag_min_distance: 5,
			prevent_default: false,
			stop_browser_behavior: false
		}).on('drag dragstart dragend dragup swipeup dragdown swipedown', function(ev){
			switch(ev.type) {
				case 'swipedown':
				case 'dragdown':
					if (header.hasClass('hide')) {
						header.removeClass('hide');
					}
					break;
				case 'dragup':
				case 'swipeup':
					if (!header.hasClass('hide')) {
						header.addClass('hide');
					}
					break;
				default:
					switch(ev.gesture.direction) {
					case 'up':
						header.addClass('hide');
						break;
					case 'down':
						header.removeClass('hide');
						break;
				}
			}
		});
	}
}