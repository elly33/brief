if(!window.application){
	window.application = {}
}

window.application.initMaps = function(){
	'use strict';

	if(typeof google == 'undefined'){return;}

	function handle(delta) {
		var zoom = mapsArr[0].getZoom() + delta,
			coordArr = false, styleMap = false;
		if (zoom > 20) {
			zoom = 10;
			refreshState();
			coordArr = getCoord();
			styleMap = getStyle();
		} else if (zoom < 10){
			zoom = 20;
			refreshState(links.filter('.' + activeClass).index() - 1);
			coordArr = getCoord();
			styleMap = getStyle();
		}
		for (var i = 0; i < mapsArr.length; i++) {
			if (coordArr) {
				mapsArr[i].panTo(coordArr);
				mapsArr[i].mapTypes.set("pink", styleMap);
			}
			mapsArr[i].setZoom(zoom);
		}
	}
	
	function setMapStyle(){
		var arr = [], i;
		for (i = 0; i < markers.length; i++){
			arr[i] = [
				{
					featureType: 'all',
					elementType: 'all',
					stylers: [
						{hue: markers.eq(i).attr('data-color')},
						{saturation: 20},
						{lightness: 10}
					]
				},
				// {
				// 	featureType: 'road',
				// 	elementType: 'geometry.stroke',
				// 	stylers: [
				// 		{color: '#ad9493'},
				// 		{weight: '0.2'}
				// 	]
				// },
				{
					featureType: 'road',
					elementType: 'geometry.fill',
					stylers: [
						{color: '#ffffff'}
					]
				},
				{
					featureType: 'water',
					elementType: 'geometry.fill',
					stylers: [
						{color: '#ffffff'}
					]
				},
				// {
				// 	featureType: 'all',
				// 	elementType: 'labels.text',
				// 	stylers: [
				// 		{color: '#ad9493'}
				// 	]
				// },
				// {
				// 	featureType: 'landscape',
				// 	elementType: 'geometry.fill',
				// 	stylers: [
				// 		{color: '#ece6ff'}
				// 	]
				// },
				{
					featureType: 'all',
					elementType: 'labels.text.stroke',
					stylers: [
						{color: '#ffffff'}
					]
				},
				{
					featureType: 'road',
					elementType: 'labels',
					stylers: [
						{visibility: 'off'}
					]
				}
			];
		}
		return arr;
	}

	function refreshState(step){
		var cur;
		if (typeof step === 'number') {
			markers.removeClass(activeClass).eq(step).addClass(activeClass);
		} else {
			cur = markers.filter('.' + activeClass);
			markers.removeClass(activeClass);
			markers.eq(cur.index() + 1).addClass(activeClass);
		}
		links.removeClass(activeClass).eq(markers.filter('.' + activeClass).index()).addClass(activeClass);
	}

	function getCurrentStep(init){
		var cur;
		if (init === true) {
			cur = markers.filter('.' + activeClass);
			if (cur.length != 1) {
				cur = markers.removeClass(activeClass).eq(0).addClass(activeClass)
			}
		} else {
			cur = markers.filter('.' + activeClass);
		}
		if (!cur.length) {
			cur = markers.removeClass(activeClass).eq(0).addClass(activeClass);
			refreshState(0);
		}
		return cur.index();
	}

	function getCoord(init){
		var cur = markers.eq(getCurrentStep(init));
		return new google.maps.LatLng(cur.attr('data-longitude') || 55.74011, cur.attr('data-latitude') || 37.6467);
	}

	function getStyle(init){
		return new google.maps.StyledMapType(featureOpts[getCurrentStep(init)], {
			name: 'Custom Style'
		});
	}

	var maps = jQuery('.map .placeholder .holder').empty(),
		overlay = jQuery('.map .markers'),
		markers = jQuery('.map .markers li'),
		activeClass = 'active',
		links = jQuery('#map-nav li'),
		featureOpts = setMapStyle(),
		curpositionx = 0,
		curpositiony = 0,
		onMoveFunc = null,
		isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		overlayMousewheel = null, mapsArr = [];
	maps.each(function(){
		var mapOptions = {
			zoom: 14,
			center: getCoord(true),
			mapTypeId: "pink",
			panControl: false,
			zoomControl: false,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			draggable: false
		},
		map = new google.maps.Map(this, mapOptions);
		map.mapTypes.set("pink", getStyle(true));
		mapsArr.push(map);
	});

	overlayMousewheel = function(event) {
		var delta = 0;
		if (!event) event = window.event;
		if (event.wheelDelta) {
			delta = event.wheelDelta/120;
		} else if (event.detail) {
			delta = -event.detail/3;
		}
		if (delta)
			handle(delta > 0 ? 1 : -1);
			if (event.preventDefault)
				event.preventDefault();
		event.returnValue = false;
	};

	if (overlay[0].addEventListener)
		overlay[0].addEventListener('DOMMouseScroll', overlayMousewheel, false);
	overlay[0].onmousewheel = overlay[0].onmousewheel = overlayMousewheel;

	links.on({
		'click': function(event){
			event.preventDefault();
			links.removeClass(activeClass);
			refreshState(jQuery(this).addClass(activeClass).index());
			var zoom = 10,
				coordArr = getCoord(),
				styleMap = getStyle();
			for (var i = 0; i < mapsArr.length; i++) {
				if (coordArr) {mapsArr[i].panTo(coordArr);}
				mapsArr[i].setZoom(zoom);
				mapsArr[i].mapTypes.set("pink", styleMap);
			}
		}
	});
	if (isTouchDevice && typeof jQuery.fn.hammer == 'function') {
		overlay.hammer({
			drag_block_horizontal: true,
			drag_block_vertical: true,
			drag_min_distance: 10,
			stop_browser_behavior: false
		}).on('touch drag swipe', function(ev){
			switch (ev.type) {
				case 'touch':
					curpositionx = parseInt(ev.gesture.center.pageX);
					curpositiony = parseInt(ev.gesture.center.pageY);
					break;
				case 'swipe':
				case 'drag':
					event.preventDefault();
					for (var i = 0; i < mapsArr.length; i++) {
						mapsArr[i].panBy((parseInt(ev.gesture.center.pageX) - curpositionx) || 0, (parseInt(ev.gesture.center.pageY) - curpositiony) || 0);
					}
					curpositionx = parseInt(ev.gesture.center.pageX);
					curpositiony = parseInt(ev.gesture.center.pageY);
					break;
			}
		});
	} else {
		overlay.on('mousedown', function(event) {
			curpositionx = event.pageX;
			curpositiony = event.pageY;
			onMoveFunc = function(event) {
				for (var i = 0; i < mapsArr.length; i++) {
					mapsArr[i].panBy(event.pageX - curpositionx, event.pageY - curpositiony);
				}
				curpositionx = event.pageX;
				curpositiony = event.pageY;
			};
			overlay.on('mousemove', onMoveFunc);
		});
		overlay.on('mouseup', function() {
			overlay.off('mousemove', onMoveFunc);
		});
	}
}